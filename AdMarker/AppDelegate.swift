//
//  AppDelegate.swift
//  AdMarker
//
//  Created by Neil Hodgson on 21/11/16.
//  Copyright © 2016 Neil Hodgson. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

  @IBOutlet weak var window: NSWindow!

  @IBOutlet var recordingsController: NSArrayController!

  @IBOutlet var tableView: NSTableView!

  @objc dynamic var items = [Recording]()

  var cmd = CommandRun()

  var scanning = true

  var indexMarking = 0
  var numberMarking = ""

  let markCommand = "/Library/Application\\ Support/ETVComskip/bin/MarkCommercials"

  var timer = Timer()

  func applicationDidFinishLaunching(_ aNotification: Notification) {
    timer = Timer.scheduledTimer(timeInterval: 1, target: self,
                                 selector: #selector(self.updateMarking),
                                 userInfo: nil, repeats: true)
  }

  func applicationWillTerminate(_ aNotification: Notification) {
  }

  static func parseLine(_ line: String) ->String {
    var ret = ""
    // 0:52:09 - 78234 frames in 205.27 sec(381.13 fps), 1.00 sec(485.00 fps), 88%
    if let framesIn = line.range(of: " frames in ") {
      let secsPlus = line[framesIn.upperBound...]
      if let sec = secsPlus.range(of: " sec(") {
        var seconds = String(secsPlus[..<sec.lowerBound])
        if let secondsFloat = Float(seconds) {
          let secondsRounded = Int(secondsFloat.rounded())
          seconds = String(secondsRounded)
        }
        let afterSeconds = secsPlus[sec.upperBound...]
        if let sec2 = afterSeconds.range(of: " sec(") {
          let rest = afterSeconds[sec2.upperBound...]
          if let commaSpace = rest.range(of: ", ") {
            let fin = rest[commaSpace.upperBound...]
            ret = fin + "  " + seconds
            //NSLog("Progress: \(ret)")
          }
        }
      }
    }
    return ret;
  }

  func progressOf(_ ident: String) -> String {
    guard let rawHome = getenv("HOME") else {
      return "";
    }
    guard let home = String(utf8String: rawHome) else {
      return "";
    }
    var ret = ""
    let path = "\(home)/Library/Logs/ETVComskip/\(ident)_comskip.log";
    do {
      let data = try String(contentsOfFile: path, encoding: .utf8)
      let lines = data.components(separatedBy: .newlines)
      for s in lines {
        if s.contains("fps),") {
          ret = AppDelegate.parseLine(s)
        }
      }
    } catch {
      print(error)
    }
    return ret;
  }

  @objc func updateMarking() {
    if !scanning {
      if numberMarking != "" {
        let prog = progressOf(numberMarking)
        items[indexMarking].status = "Marking " + prog
      }
    }
  }

  @IBAction func scan(sender: AnyObject?) {
    if !cmd.isRunning() {
      items = [Recording]()
      scanning = true
      NSLog("scan")
      //runTask(cmd: "ls -lat merc")
      runTask(command: markCommand)
      //runTask(command: "cat merc/etv.txt")
      //runTask(cmd: "bash ga.sh")
    }
  }

  @IBAction func mark(sender: AnyObject?) {
    if !cmd.isRunning() {
      scanning = false
      indexMarking = tableView.selectedRow
      NSLog("mark \(indexMarking)")
      if indexMarking >= 0 {
        numberMarking = items[indexMarking].number
        let prog = progressOf(numberMarking)
        items[indexMarking].status = "Marking " + prog
        let markThis = markCommand + " --force --log " + numberMarking
        NSLog("marking \(prog)")
        runTask(command: markThis)
      }
    }
  }

  func completedTask() {
    NSLog("completed")
    let arrayOfRun = cmd.textOfRun.components(separatedBy: "\n")
    if (scanning) {
      for s in arrayOfRun {
        if s.contains(" = [") {
          items.append(Recording(text: s))
          //debugPrint(s)
        }
      }
      items.sort(by: { $0.name < $1.name })
    } else {
      // Marking completed
      numberMarking = ""
      items[indexMarking].status = "Finished"
    }
    cmd.textOfRun = ""
  }

  func runTask(command: String) {
    if cmd.isRunning() {
      return
    }

    cmd.start(command: command, completed: completedTask)
  }
}
