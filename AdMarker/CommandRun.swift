//
//  CommandRun.swift
//  AdMarker
//
//  Created by Neil Hodgson on 23/11/16.
//  Copyright © 2016 Neil Hodgson. All rights reserved.
//

import Foundation

class CommandRun {
  var task: Process?
  var exitStatus: Int32 = 0
  var running = false
  var textOfRun = ""
  var observeTerminate : Any?
  var observeData : Any?
  var completion : (() -> Void)?

  init() {

  }

  func start(command: String, completed: (() -> Void)?) {
    completion = completed
    let t = Process()
    task = t

    textOfRun = ""

    running = true
    t.launchPath = "/bin/bash"
    t.arguments = ["-c", command]
    guard let rawHome = getenv("HOME") else {
      return;
    }
    guard let home = String(utf8String: rawHome) else {
      return;
    }
    t.currentDirectoryPath = home

    let pipeIn = Pipe()
    t.standardInput = pipeIn

    let outputPipe = Pipe()
    t.standardOutput = outputPipe
    t.standardError = outputPipe

    let fhRead = outputPipe.fileHandleForReading

    let center = NotificationCenter.default

    observeTerminate =
      center.addObserver(forName: Process.didTerminateNotification,
                         object: t, queue: nil, using: finishedTask)

    observeData =
      center.addObserver(forName:FileHandle.readCompletionNotification,
                         object: fhRead, queue: nil, using: receivedData)

    fhRead.readInBackgroundAndNotify()

    t.launch()
  }

  func isRunning() -> Bool {
    return running || (task != nil)
  }

  func finishedTask(notification: Notification) {
    exitStatus = task?.terminationStatus ?? 0
    task = nil
    if !isRunning(), let completion = completion {
      completion()
    }
  }

  func receivedData(notification: Notification) {
    NSLog("receivedData")
    guard let dataItem = notification.userInfo?[NSFileHandleNotificationDataItem] else {
      return
    }
    guard let data = dataItem as? Data else {
      return
    }
    if data.count > 0 {
      if let s = String(data: data, encoding: String.Encoding.isoLatin1) {
        textOfRun = textOfRun + s
        debugPrint(s)
      }
      if let fHandle = notification.object as? FileHandle {
        fHandle.readInBackgroundAndNotify()
      }
    } else {
      running = false
      if task == nil, let completion = completion {
        completion()
      }
    }
  }
  
}

