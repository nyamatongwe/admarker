//
//  Recording.swift
//  AdMarker
//
//  Created by Neil Hodgson on 22/11/16.
//  Copyright © 2016 Neil Hodgson. All rights reserved.
//

import Cocoa

class Recording: NSObject {
  @objc dynamic var number = ""
  @objc dynamic var name = ""
  @objc dynamic var channelNumber = ""
  @objc dynamic var channelName = ""
  @objc dynamic var status = ""
  init(text:String) {
    let csSpace = CharacterSet(charactersIn: " \t")
    let csBracket = CharacterSet(charactersIn: "]")
    //  499692000 = [Resistance - 1.02 1.02], [30], [SBS HD]
    let trimmed = text.trimmingCharacters(in: csSpace)
    if let space = trimmed.range(of: " = [") {
      number = String(trimmed[..<space.lowerBound])
      let afterNum = trimmed[space.upperBound...]
      if let brack = afterNum.range(of: "], [") {
        name = String(afterNum[..<brack.lowerBound])
        let afterName = afterNum[brack.upperBound...]
        if let brack2 = afterName.range(of: "], [") {
          channelNumber = String(afterName[..<brack2.lowerBound])
          channelName = afterName[brack2.upperBound...].trimmingCharacters(in: csBracket)
          //status = "|" + name.uppercased()
        }
      }
    }
    super.init()
  }
}
