//
//  AdMarkerTests.swift
//  AdMarkerTests
//
//  Created by Neil Hodgson on 21/11/16.
//  Copyright © 2016 Neil Hodgson. All rights reserved.
//

import XCTest
@testable import AdMarker

class AdMarkerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testRecording() {
      let x = Recording(text: "499692000 = [Resistance - 1.02 1.02], [30], [SBS HD]")
      XCTAssert(x.channelNumber == "30")
      XCTAssert(x.channelName == "SBS HD")
      XCTAssert(x.name == "Resistance - 1.02 1.02")
      XCTAssert(x.number == "499692000")
    }
    
  func testParseLine() {
    // 0:52:09 - 78234 frames in 205.27 sec(381.13 fps), 1.00 sec(485.00 fps), 88%
    let x = AppDelegate.parseLine("0:52:09 - 78234 frames in 205.27 sec(381.13 fps), 1.00 sec(485.00 fps), 88%")
    XCTAssert(x == "88%  205")
  }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
